package com.company;

import java.util.Random;
import java.util.Scanner;

public class EnemyGenerator {
    Scanner in = new Scanner(System.in);
    Random rand = new Random();

    String[] enemies = { "Skeleton", "Zombie" ,"Warrior", "Assasin" };
    int maxEnemyHealth = 75;
    int enemyAttackDamage = 25;

    public int enemyHealth(){
        int ehealth = rand.nextInt(maxEnemyHealth);
        return ehealth;
    }

    public String enemyName(){
        String enemy = enemies[rand.nextInt(enemies.length)];
        return enemy;
    }







}
