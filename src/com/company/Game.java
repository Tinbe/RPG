package com.company;

import java.util.Random;
import java.util.Scanner;

public class Game {

    public void game(){
        Scanner in = new Scanner(System.in);
        Random rand = new Random();



        Player player = new Player();
        EnemyGenerator generator = new EnemyGenerator();


        boolean running = true;

        System.out.println("Welcome to the Dungeon!");

        GAME:
        while (running){


            System.out.println("-----------------------------------------------------------------");

            int enemyHealth = generator.enemyHealth();
            String enemy = generator.enemyName();
            System.out.println("\t# " + enemy + " has appeared! #\n");

            while (enemyHealth > 0 && player.getHealth() > 0){

                System.out.println("\t Your HP " + player.getHealth());
                System.out.println("\t " + enemy + "'s HP " + enemyHealth);
                System.out.println("\n\t What would you like to do?");
                System.out.println("\t 1. Attack");
                System.out.println("\t 2. Drink health potion");
                System.out.println("\t 3. Run");

                String input = in.nextLine();

                if (input.equals("1")){
                   enemyHealth = player.attack(enemyHealth,generator.enemyAttackDamage,enemy);
                }

                else if (input.equals("2")){
                    player.dringHealthPotion();
                    }

                else if (input.equals("3")){
                    System.out.println("\t You run away from the " + enemy + "!");
                    continue GAME;
                }

                else {
                    System.out.println("Invalid command");
                }





            }

            if (player.getHealth() < 1){
                System.out.println("\t # You are dead #");
                break;
            }
            System.out.println("-----------------------------------------------------------------");
            System.out.println(" # " + enemy + " was defeated! # ");
            System.out.println(" # You have  " + player.getHealth() + "HP left. #");
            if (rand.nextInt(100) < player.getHealthPotionDropChance()){
                player.setNumHealthPotions(player.getNumHealthPotions() + 1);
                System.out.println(" # The " + enemy + " dropped a health potion! #");
                System.out.println(" # You have " + player.getNumHealthPotions() + " health potion(s). # ");
            }
            System.out.println("-----------------------------------------------------------------");
            System.out.println(" What would you like to do now?");
            System.out.println("1. Continue fighting");
            System.out.println("2. Exit the dungeon");

            String input = in.nextLine();

            while (!input.equals("1") && !input.equals("2")){
                System.out.println("Invalid command");
                input = in.nextLine();
            }

            if(input.equals("1")){
                System.out.println("You continue on your adventure");
            }
            else if (input.equals("2")){
                System.out.println("You exit the dungeon!");
                break;
            }




        }

        System.out.println("######################");
        System.out.println("# THANKS FOR PLAYING #");
        System.out.println("######################");
    }



}
