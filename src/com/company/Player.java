package com.company;

import java.util.Random;

public class Player {

    Random rand = new Random();

    private int health = 100;
    private int attackDamage = 50;
    private int numHealthPotions = 3;
    private int healthPotionHealAmount = 30;
    private int healthPotionDropChance = 50; // Procenti

    public void player(){






    }
    public int attack(int enemyHealth, int enemyAttackDamage, String enemy) {

            int damageDealt = rand.nextInt(attackDamage);
            int damageTaken = rand.nextInt(enemyAttackDamage);

            enemyHealth -= damageDealt;
            health -= damageTaken;

            System.out.println("\t You hit the " + enemy + " for " + damageDealt + " damage.");
            System.out.println("\t You recieve " + damageTaken + " damage");

        return enemyHealth;
    }

    public void dringHealthPotion(){

        if (getNumHealthPotions() > 0){
            health += healthPotionHealAmount;
            numHealthPotions--;
            System.out.println("/t > You drink a health potion, healing yourself for " + healthPotionHealAmount + "."
                    + "\n\t You now have " + health + " HP."
                    + "\n\t> You have " + numHealthPotions + " health potions left. \n");
        }
        else {
            System.out.println("\t> You have no health potions left! Defeat enemies for a chance to get one");
        }

        }

















    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealthPotionHealAmount() {
        return healthPotionHealAmount;
    }

    public void setHealthPotionHealAmount(int healthPotionHealAmount) {
        this.healthPotionHealAmount = healthPotionHealAmount;
    }

    public int getNumHealthPotions() {
        return numHealthPotions;
    }

    public void setNumHealthPotions(int numHealthPotions) {
        this.numHealthPotions = numHealthPotions;
    }

    public int getHealthPotionDropChance() {
        return healthPotionDropChance;
    }
}
